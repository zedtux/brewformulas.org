FROM ruby:2.6-slim-buster
WORKDIR /application

# `deps` installs all the system libraries required by the gems in order
# to install them all.
# Finally it creates a temporary image used as a base image later.
deps:
    RUN apt-get update \
        && apt-get install -y libpq-dev \
                              build-essential \
                              nodejs \
                              git
    SAVE IMAGE

# `gems` installs all the gems from the Gemfile.lock for a production usage.
# Finally it creates a temporary image used as a base image later.
gems:
    FROM +deps

    COPY Gemfile* ./

    RUN gem install rubygems-update \
        && update_rubygems \
        && gem install bundler:1.16.0 \
        && bundle install --gemfile=/application/Gemfile \
                          --jobs=8 \
                          --without development test

    SAVE ARTIFACT /usr/local/bundle bundler

# `assets` runs the Rails assets:precompile task and create an artifact from it
# used in the `prod` target.
assets:
    FROM +deps

    ENV RAILS_ENV production

    COPY app/assets /application/app/assets
    COPY app/models /application/app/models
    COPY app/sweepers /application/app/sweepers
    COPY config/environments/production.rb /application/config/environments/production.rb
    COPY config/initializers /application/config/initializers
    COPY config/application.rb /application/config/application.rb
    COPY config/boot.rb /application/config/boot.rb
    COPY config/database.yml /application/config/database.yml
    COPY config/environment.rb /application/config/environment.rb
    COPY config/redis.yml /application/config/redis.yml
    COPY vendor /application/vendor
    COPY Gemfile* /application
    COPY Rakefile /application

    COPY +gems/bundler /usr/local/bundle

    RUN bundle exec rake assets:precompile RAILS_ENV=production

    SAVE ARTIFACT public/assets assets

# `prod` builds the production image of the project which means precompiling
# the projet assets.
# Finally it creates the Docker image used in order to run the project.
prod:
    FROM +deps

    ENV RAILS_ENV production

    COPY app /application/app
    COPY bin /application/bin
    COPY config /application/config
    COPY db /application/db
    COPY lib /application/lib
    COPY public /application/public
    COPY vendor /application/vendor
    COPY config.ru /application
    COPY Gemfile* /application
    COPY package.json /application
    COPY Rakefile /application
    COPY VERSION /application

    COPY +gems/bundler /usr/local/bundle
    COPY +assets/assets public/assets

    RUN useradd -ms /bin/bash brewformulas \
        && chown -R brewformulas:users /application

    EXPOSE 3000
    ENTRYPOINT ["bundle", "exec"]
    CMD ["bin/rails server -b 0.0.0.0"]
    SAVE IMAGE registry.gitlab.com/zedtux/brewformulas.org:latest

# `ci` is based on the production image, in order to keep an environment as
# close as possible with production when running the tests.
# Nonetheless it install additionnal gems used to run the tests.
ci:
    FROM +prod

    ENV RAILS_ENV test

    COPY features /application/features
    COPY script /application/script
    COPY spec /application/spec
    COPY .coveralls.yml /application/.coveralls.yml

    RUN bundle config --delete without \
        && bundle install --gemfile=/application/Gemfile \
                          --jobs=8 \
                          --without development
    CMD ["rake"]
    SAVE IMAGE

integration-test:
    FROM docker:19.03.13-dind

    RUN apk --update --no-cache add docker-compose \
                                    jq
    COPY docker-compose.yml ./

    WITH DOCKER
        DOCKER PULL postgres:9.6.2
        DOCKER PULL redis:3.2.8
        DOCKER PULL memcached:1.5.3-alpine
        DOCKER LOAD +ci zedtux/brewformulas.org:dev
        RUN docker-compose up -d \
            && POSTGRESQL_CONTAINER_IPADDR=$(docker inspect default_postgres_1 \
                                             | jq -r '.[0].NetworkSettings.Networks.default_default.IPAddress') \
            && for i in {1..30}; do nc -z $POSTGRESQL_CONTAINER_IPADDR 5432 && break; sleep 1; done; \
            docker run --network=host \
                       --env DATABASE_HOST=$POSTGRESQL_CONTAINER_IPADDR \
                       --env POSTGRESQL_USER=postgres \
                       --env RAILS_ENV=test \
                       zedtux/brewformulas.org:dev bash -c 'bundle exec rake db:setup \
                                                            && bundle exec rake' \
            && docker-compose down
    END

# `dev` is based on the production image, in order to keep an environmnet as
# close as possible with produciton and then installs all the other gems for
# developing the project.
# The final image is used by the local docker-compose.yml file.
dev:
    FROM +prod

    ENV RAILS_ENV development

    RUN bundle config --delete without \
        && bundle install --gemfile=/application/Gemfile \
                          --jobs=8 \
        && chown -R brewformulas:users /usr/local/bundle

    SAVE IMAGE registry.gitlab.com/zedtux/brewformulas.org:dev
