class AddsYearHitsColumnToTheHomebrewFormulasTable < ActiveRecord::Migration[5.1]
  def change
    add_column :homebrew_formulas, :yearly_hits, :string

    Homebrew::Formula.all.each do |formula|
      formula.update(yearly_hits: [0,0,0,0,0,0,0,0,0,0,0])
    end

    change_column_null :homebrew_formulas, :yearly_hits, false
  end
end
