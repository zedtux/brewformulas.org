class AddExternalColumnToHomebrewFormulasTable < ActiveRecord::Migration[4.2]
  def change
    add_column :homebrew_formulas, :external, :boolean, default: false, null: false
    add_index :homebrew_formulas, :external
  end
end
