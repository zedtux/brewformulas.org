class AddTouchedOnColumnToHomebrewFormulasTable < ActiveRecord::Migration[4.2]
  def change
    add_column :homebrew_formulas, :touched_on, :date
    execute "UPDATE homebrew_formulas SET touched_on = '#{Date.yesterday}'"
  end
end
