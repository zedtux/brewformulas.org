class AddDescriptionAutomaticColumnToHomebrewFormulasTable < ActiveRecord::Migration[4.2]
  def change
    add_column :homebrew_formulas, :description_automatic, :boolean, default: false
  end
end
