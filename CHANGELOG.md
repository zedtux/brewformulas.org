# ChangeLog

## 2.1.0 (2017-11-21)

 - Memcache on homepage
 - Handle formula names with an @ symbol (Fixes #129)

## 2.0.0 (2017-05-31)

 - Rails 5
 - Bootstrap 4

## 1.0.0

 - Rails 4
 - Bootstrap 3
